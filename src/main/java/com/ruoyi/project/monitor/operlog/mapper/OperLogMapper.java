package com.ruoyi.project.monitor.operlog.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.project.monitor.operlog.domain.OperLog;
import com.ruoyi.project.system.config.domain.Config;
import org.apache.ibatis.annotations.Param;

/**
 * 操作日志 数据层
 * 
 * @author ruoyi
 */
public interface OperLogMapper
{
    /**
     * 新增操作日志
     * 
     * @param operLog 操作日志对象
     */
    void insertOperlog(OperLog operLog);

    /**
     * 查询系统操作日志集合
     * 
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    Page<OperLog> selectOperLogList(@Param("page") IPage<OperLog> page, @Param("operLog") OperLog operLog);

    List<OperLog> selectOperLogList(@Param("operLog")OperLog operLog);
    
    /**
     * 批量删除系统操作日志
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
    int deleteOperLogByIds(String[] ids);
    
    /**
     * 查询操作日志详细
     * 
     * @param operId 操作ID
     * @return 操作日志对象
     */
    OperLog selectOperLogById(Long operId);
    
    /**
     * 清空操作日志
     */
    void cleanOperLog();
}
